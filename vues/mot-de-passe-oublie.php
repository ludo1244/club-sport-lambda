<?php
    include("modules/partie1.php");

    if(isset($_SESSION["user"])){
        $_SESSION["error"] = "Vous etes déjà connecté a un compte";
        echo '<script type="text/javascript">';
        echo 'window.location.href="index.php"';
        echo '</script>';
    }
?>

<div class="container card text-center">
    <h1 class="card-header">Mot de passe oublié :</h1>
    <div class="card-body">
        <form class="" action="process/motDePasseOublie.php" method="POST">
            <div class="row form-group">
                <label for="email" class="col-4">Email</label>
                <div class="col-md-8">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-dark" type="submit">Modifier mot de passe</button>
            </div>
        </form>   
    </div>
</div>
<?php
    include("modules/partie3.php")
?>