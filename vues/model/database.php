<?php
//class 
include_once(dirname(__FILE__)."/user.php");
include_once(dirname(__FILE__)."/seance.php");
include_once(dirname(__FILE__)."/commentaire.php");

class Database{

    //constante de connexion
    const DB_HOST = "mariadb";
    const DB_PORT = "3306";
    const DB_DATABASE_NAME = "clubSportLambda";
    const DB_USER = "adminClub";
    const DB_PASSWORD = "2Bit8DL9x";

    private $connexion;

    //initialiser la connexion
    public function __construct(){
        $this->connexion = new PDO(
            "mysql:host=" . self::DB_HOST . ";dbname=" . self::DB_DATABASE_NAME . ";",
            self::DB_USER,
            self::DB_PASSWORD
        );
    }
/****** SEANCES ******************************************************************************/ 
    // fonction pour inserer une séance dans la BD
    public function createSeance(Seance $seance){
        // Etape 1 : préparation de la requête
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO seances(titre, description, heureDebut, date, duree, nbParticipantsMax, couleur) VALUES (:titre, :description, :heureDebut, :date, :duree, :nbParticipantsMax, :couleur)"
        );
        // Etape 2 : exécution de la requête (avec paramètres)
        $pdoStatement->execute([
            "titre"    => $seance->getTitre(),
            "description"       => $seance->getDescription(),
            "heureDebut" => $seance->getHeureDebut(),
            "date"     => $seance->getDate(),
            "duree"     => $seance->getDuree(),
            "nbParticipantsMax"     => $seance->getNbParticipantsMax(),
            "couleur"     => $seance->getCouleur()
        ]);
        if($pdoStatement->errorCode()== 0){
            return $this->connexion->lastInsertId();
        }else{
            return false;
        }
        
    }
   
    //Chercher un séance par id
    public function getSeanceById($id){
        // Etape 1 : préparation de la requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM seances WHERE id = :id"
        );
        // Etape 2 : exécution de la requête (avec paramètres)
        $pdoStatement->execute([
            "id"    => $id
        ]);
        $seance = $pdoStatement->fetchObject("Seance");
        return $seance;

    }
    // return toutes les séances de la semaine
    public function getSeanceByWeek($week){
        // Etape 1 : préparation de la requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM seances WHERE WEEKOFYEAR(date) = :week ORDER BY date, heureDebut"
        );
        $pdoStatement->execute([
            "week"    => $week
        ]);
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Seance");
        return $seances;
    }
    // supprimer toutes les seances
    public function deleteAllSeance(){
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances;"
        ); 
        $pdoStatement->execute();
    }
    //fonction pour mettre a jour une séance
    public function updateSeance(Seance $seance){
        // Etape 1 : préparation de la requête
        $pdoStatement = $this->connexion->prepare(
            "UPDATE seances SET titre = :titre, description = :description, heureDebut = :heureDebut, date = :date, duree = :duree, nbParticipantsMax = :nbParticipantsMax, couleur = :couleur WHERE id = :id"
        );
        $pdoStatement->execute([
            "titre"    => $seance->getTitre(),
            "description"       => $seance->getDescription(),
            "heureDebut" => $seance->getHeureDebut(),
            "date"     => $seance->getDate(),
            "duree"     => $seance->getDuree(),
            "nbParticipantsMax"     => $seance->getNbParticipantsMax(),
            "couleur"     => $seance->getCouleur(),
            "id"        => $seance->getId()
        ]);
        if($pdoStatement->errorCode()== 0){
            return true;
        }else{
            return false;
        }
    }
    // fonction pour inserer un participant a une séance
    public function insertParticipant($idSeance, $idUser){
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO inscrits (id_user, id_seance) VALUES (:id_user, :id_seance)"
        );
        $pdoStatement->execute([
            "id_user"    => $idUser,
            "id_seance"       => $idSeance,
        ]);
        if($pdoStatement->errorCode()== 0){
            return true;
        }else{
            return false;
        }
    }
    // fonction pour enlever un participant a une séance
    public function deleteParticipant($idSeance, $idUser){
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits WHERE id_user = :id_user AND id_seance = :id_seance"
        );
        $pdoStatement->execute([
            "id_user"    => $idUser,
            "id_seance"       => $idSeance,
        ]);
        if($pdoStatement->errorCode()== 0){
            return true;
        }else{
            return false;
        }
    }
    public function deleteSeance($id){
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances WHERE id = :id"
        );
        $pdoStatement->execute([
            "id"    => $id,
        ]);  
        if($pdoStatement->errorCode()== 0){
            return true;
        }else{
            return false;
        }      
    }
/****** USERS ******************************************************************************/
    // fonction pour rajouter un USERS
    public function createUser(User $user){
            // Etape 1 : préparation de la requête
            $pdoStatement = $this->connexion->prepare(
                "INSERT INTO users(nom, email, password, isAdmin, isActif, token) VALUES (:nom, :email, :password, :isAdmin, :isActif, :token)"
            );
            // Etape 2 : exécution de la requête (avec paramètres)
            $pdoStatement->execute([
                "nom"    => $user->getNom(),
                "email"       => $user->getEmail(),
                "password" => $user->getPassword(),
                "isAdmin"     => $user->getAdmin(),
                "isActif"     => $user->getActif(),
                "token"     => $user->getToken(),
            ]);
            if($pdoStatement->errorCode()== 0){
                return $this->connexion->lastInsertId();
            }else{
                return false;
            }
    }
    //fonction qui modifie unlle nom d'un user
    public function updateNameUser($id,$nom){
        $pdoStatement = $this->connexion->prepare(
            "UPDATE users SET nom = :nom WHERE id = :id "
        );
        $pdoStatement->execute([
            "id"     => $id,
            "nom"    => $nom,
        ]);
        if($pdoStatement->errorCode()== 0){
            return true;
        }else{
            return false;
        }
    }
    //fonction qui modifie le mot de passe d'un user
    public function updateMDPUser($id,$password){
        $pdoStatement = $this->connexion->prepare(
            "UPDATE users SET password = :password WHERE id = :id "
        );
        $pdoStatement->execute([
            "id"     => $id,
            "password"    => $password,
        ]);
        if($pdoStatement->errorCode()== 0){
            return true;
        }else{
            return false;
        }
    }
    //Chercher un user par id
    public function getUserById($id){
        // Etape 1 : préparation de la requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM users WHERE id = :id"
        );
        // Etape 2 : exécution de la requête (avec paramètres)
        $pdoStatement->execute([
            "id"    => $id
        ]);
        $user = $pdoStatement->fetchObject("User");
        return $user;

    }
    //activer un user
    public function activateUser($id){
        $pdoStatement = $this->connexion->prepare(
            "UPDATE users SET isActif = 1 WHERE id = :id"
        );
        $pdoStatement->execute([
            "id"    => $id
        ]);
        if($pdoStatement->errorCode()== 0){
            return true;
        }else{
            return false;
        }
    }
    //Chercher un user par email
    public function getUserByEmail($email){
        // Etape 1 : préparation de la requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM users WHERE email = :email"
        );
        // Etape 2 : exécution de la requête (avec paramètres)
        $pdoStatement->execute([
            "email"    => $email
        ]);
        $user = $pdoStatement->fetchObject("User");
        return $user;

    }
    //fonction qui donne les séances d'un user
    public function getSeanceByUserId($idUser){
        $pdoStatement = $this->connexion->prepare(
            "SELECT s.* FROM seances s INNER JOIN inscrits i ON s.id = i.id_seance WHERE i.id_user = :id_user"
        );
        $pdoStatement->execute([
            "id_user"    => $idUser
        ]); 
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Seance");
        return $seances;
    }
    //fonction test si email deja utiliser
    public function isEmailExists($email){
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM users WHERE email = :email"
        );
        $pdoStatement->execute([
            "email"    => $email
        ]); 
        $nbUser = $pdoStatement->fetchColumn();
        if($nbUser == 0){
            return false;
        }else{
            return true;
        }
    }
//fonction supprimer tout les users
    public function deleteAllUser(){
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM users"
        );
        $pdoStatement->execute();
    }
    //fonction pour supprimer tout les inscriptions
    public function deleteAllInscrit(){
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits"
        );
        $pdoStatement->execute();
    }


    //fonction qui test si inscrit
    public function isInscrit($idUser, $idSeance){
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM inscrits WHERE id_user = :id_user AND id_seance = :id_seance"
        );
        $pdoStatement->execute([
            "id_user"    => $idUser,
            "id_seance"    => $idSeance,
        ]); 
        $inscrit = $pdoStatement->fetchColumn();
        if($inscrit == 0){
            return false;
        }else{
            return true;
        }
    }

    //fonction qui donne le nbr d'inscrit a une seance
    public function nombreInscrit($idSeance){
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM inscrits WHERE id_seance = :id_seance"
        );
        $pdoStatement->execute([
            "id_seance"    => $idSeance,
        ]); 
        $nbInscrit = $pdoStatement->fetchColumn();
        return $nbInscrit;
    }
/**  commentaires ********************************************************************************/
    //fonction pour recuperer les commentaires par seance
    public function getCommentaireById($id){
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM commentaires WHERE id = :id"
        );
        $pdoStatement->execute([
            "id"    => $id,
        ]);
        $commentaire = $pdoStatement->fetchObject("Commentaire");
        if($pdoStatement->errorCode()== 0){
            return $commentaire;
        }else{
            return false;
        }
    }
    //fonction pour recuperer les commentaires par seance
    public function getCommentaireByIdSeance($idSeance){
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM commentaires WHERE id_seance = :id_seance"
        );
        $pdoStatement->execute([
            "id_seance"    => $idSeance,
        ]);
        $commentaires = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Commentaire");
        if($pdoStatement->errorCode()== 0){
            return $commentaires;
        }else{
            return false;
        }
    }
    public function addCommentaire(Commentaire $commentaire){
            // Etape 1 : préparation de la requête
            $pdoStatement = $this->connexion->prepare(
                "INSERT INTO commentaires(date, sujet, message, id_seance, id_user) VALUES (:date, :sujet, :message, :idSeance, :idUser)"
            );
            // Etape 2 : exécution de la requête (avec paramètres)
            $pdoStatement->execute([
                "date"    => $commentaire->getDate(),
                "sujet"       => $commentaire->getSujet(),
                "message" => $commentaire->getMessage(),
                "idSeance"     => $commentaire->getIdSeance(),
                "idUser"     => $commentaire->getIdUser()
            ]);
            if($pdoStatement->errorCode()== 0){
                return $this->connexion->lastInsertId();
            }else{
                return false;
            }
    }
    // fonction pour enlever un commentaire
    public function deleteCommentaire($idCommentaire){
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM commentaires WHERE id = :id"
        );
        $pdoStatement->execute([
            "id"    => $idCommentaire,
        ]);
        if($pdoStatement->errorCode()== 0){
            return true;
        }else{
            return false;
        }
    }






}

?>