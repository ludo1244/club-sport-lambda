<?php

//process formulaire login

session_start();
require_once(__DIR__."/../model/database.php");
$database = new Database();

//recupere les donnees du form
$email = isset($_POST["email"]) ? $_POST["email"] : NULL;
$password = isset($_POST["password"]) ? $_POST["password"] : NULL;


//verifications
if($email == NULL || $password == NULL){
    $_SESSION["error"] = "L'email et le mot de passe sont obligatoires";
    header("location: ../login.php");
    exit();
}

$user = $database->getUserByEmail($email);

//si l'utilisateur a ete trouvé
if(!$user){
    $_SESSION["error"] = "L'email est incorrect, vous n'avez pas été trouvé";
    header("location: ../login.php");
    exit(); 
}
//on verifie que l'utilisateur est actif
if($user->getActif() == 0){
    $_SESSION["error"] = "Votre compte n'a pas été validé, consultez vos emails";
    header("location: ../login.php");
    exit(); 
}
if(!password_verify($password, $user->getPassword())){
    $_SESSION["error"] = "Le mot de passe est incorrect";
    header("location: ../login.php");
    exit();     
}

// si tous les tests ont réussi on peut log le user
$_SESSION["id"] = $user->getId();
$_SESSION["user"] = serialize($user);

// on redirige vers la page planning
$_SESSION["info"] = "vous etes bien connecté";
header("location: ../planning.php");