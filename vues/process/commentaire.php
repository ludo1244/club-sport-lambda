<?php
//demarrer la session pour les messages
session_start();

require_once(__DIR__."/../model/database.php");
$database = new Database();

$sujet = isset($_POST["sujet"]) ? $_POST["sujet"] : null;
$message = isset($_POST["message"]) ? $_POST["message"] : null;
$idSceance = isset($_GET["idSceance"]) ? $_GET["idSceance"] : null;
$date = date("y.m.d");
$user = unserialize($_SESSION["user"]);

$error = "";
if($sujet == null){
    $error .= "Le sujet doit etre donné";
}
if($message == null){
    $error .= "Il faut ecrire quelque chose pour commenter";
}
if($idSceance == null){
    $error .= "Problème, veuillez reessayer !";
}
//en cas d'erreurs rediriger vers le formulaire
if (!empty($error)){
    $_SESSION["error"] = $error;
    header("location: ../cours.php?id=".$idSceance);
    exit;
}


$commentaire = Commentaire::createCommentaire($date,$sujet,$message,$user->getId(), $idSceance);
$idCommentaire = $database->addCommentaire($commentaire);

// on doit envoyer un email
if ($idCommentaire){
    //on redirige vers l'ecran de login avec un message
    $_SESSION["info"] = "Votre commentaire a bien été prise en compte";
    header("location: ../cours.php?id=".$idSceance);
}else{
    $_SESSION["error"] = "Une erreur s'est produite lors de votre commentaire. veuillez recommencer le processus.";
    header("location: ../cours.php?id=".$idSceance);
}
