<?php
// ce fichier sert a supprimer une séance
//session pour passer les message d'une page a l'autre
session_start();

require_once("../model/database.php");
$database = new Database();

$idCommentaire = $_GET["id"];

if(!$idCommentaire){
    $_SESSION["error"] = "Le lien de suppression n'est pas correct";
    header("location: ../planning.php");
}

$commentaire = $database->getCommentaireById($idCommentaire);
$idCours = $commentaire->getIdSeance();
$user = unserialize($_SESSION["user"]);

//verifie si lutilisateur peut supprime et supprime
if ($user != null){
    if ($user->getId()==$commentaire->getIdUser()||$user->getAdmin()==1){

        // si tout vas bien on supprime la seance
        if ($database->deleteCommentaire($idCommentaire)){
            $_SESSION["info"] = "Commentaire supprimée avec succès";
            header("location: ../cours.php?id=".$idCours);
        }else{
            $_SESSION["error"] = "Erreur lors de la suppression";
            header("location: ../cours.php?id=".$idCours);
        }
    }
}

$_SESSION["error"] = "Erreur lors de la suppression";
header("location: ../cours.php?id=".$idCours);