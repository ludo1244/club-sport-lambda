<?php

//ceci remplace l'instruction quand on défini un namespace à la classe
use PHPUnit\Framework\TestCase;

include_once(__DIR__."/../vues/model/seance.php");
include_once(__DIR__."/../vues/model/user.php");
include_once(__DIR__."/../vues/model/database.php");

final class UserTest extends TestCase{
    public function testCreateUser(){
        $user = User::createUser("toto","toto@gmail.com", password_hash("1234",PASSWORD_DEFAULT),0,0,bin2hex(random_bytes(20)));
        $database = new Database();
        $this->assertNotFalse($database->createUser($user));
        
    }
    public function testGetAndActivateUser(){
        $database = new Database();
        $user = User::createUser("toto","toto@gmail.com", password_hash("1234",PASSWORD_DEFAULT),0,0,bin2hex(random_bytes(20)));
        $id = $database->createUser($user);
        $this->assertNotFalse($id);
        $this->assertTrue($database->activateUser($id));

        $user = $database->getUserById($id);
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals(1, $user->getActif());

    }
    public function testEmailAlreadyExists(){
        $database = new Database();
        $user = User::createUser("toto","toto@gmail.com", password_hash("1234",PASSWORD_DEFAULT),0,0,bin2hex(random_bytes(20)));

        $this->assertNotFalse($database->createUser($user));

        $emailTrue = "toto@gmail.com";
        $this->assertTrue($database->isEmailExists($emailTrue));

        $emailFalse = "toto@hotmail.com";
        $this->assertFalse($database->isEmailExists($emailFalse));
    }
    public function testGetUserByEmail(){
        $database = new Database();
        $user = User::createUser("toto","toto@gmail.com", password_hash("1234",PASSWORD_DEFAULT),0,0,bin2hex(random_bytes(20)));

        $this->assertNotFalse($database->createUser($user));

        $emailTrue = "toto@gmail.com";
        $this->assertInstanceOf(User::class, $database->getUserByEmail($emailTrue));

        $emailFalse = "toto@hotmail.com";
        $this->assertFalse($database->getUserByEmail($emailFalse));
    }





    public static function tearDownAfterClass(){
        $database = new Database();
        $database->deleteAllInscrit();
        $database->deleteAllUser();
        $database->deleteAllSeance();
    }
}