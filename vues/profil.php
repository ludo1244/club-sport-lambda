<?php
    include("modules/partie1.php");
?>
<?php
    if(!isset($_SESSION["user"])){
        echo '<script type="text/javascript">';
        echo 'window.location.href="login.php"';
        echo '</script>';
    }

    require_once(__DIR__."/../vues/model/database.php");
    $database = new Database();

    $user = unserialize($_SESSION["user"]);

    $seances = $database->getSeanceByUserId($user->getId());

?>
<div class="container card text-center mt-4">
    <h1 class="card-header">Bienvenue <?php echo $user->getNom() ?></h1>
    <div class="card-body text-left">
        <div class="card-title">
            Vous etes inscrit aux cours suivant
        </div>
        <div class="card-text">
            <ul class="inscrit text-dark text-left">
                <?php foreach($seances as $seance ){?>
                    <a href="/vues/cours.php?id=<?php echo $seance->getId(); ?>">
                    <li> <i class="fa fa-bookmark"></i>
                    <?php echo $seance->getTitre(). ", le ".date("d/m/Y",strtotime($seance->getDate()))." à ". date("G\hi", strtotime($seance->getHeureDebut()))?>
                    </li>
                    </a>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<div class="container card text-center mt-4">
    <h1 class="card-header">Vos informations</h1>
    <div class="card-body text-left">
       <!-- <div class="card-title">
            Modifier que ce que vous voulez changer !
        </div>-->
        <div class="card-text">
            <form class="text-md-right" action="process/modificationNom.php" method="POST">
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label">Nom</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="<?php echo $user->getNom() ?>">
                    </div>
                </div>
                <div class="form-group text-center">
                    <button class="btn btn-dark" type="submit">Modifier le nom</button>
                </div>
            </form>
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-right">Email</label>
                <div class="col-md-8">
                    <label class="col-md-4 col-form-label"><?php echo $user->getEmail() ?></label>
                </div>
            </div>
            <form class="text-md-right" action="process/modificationMDP.php" method="POST">
                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label">Mot de passe</label>
                    <div class="col-md-8">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password-repeat" class="col-md-4 col-form-label">Retapez le mot de passe</label>
                    <div class="col-md-8">
                        <input type="password" class="form-control" id="password-repeat" name="password-repeat" placeholder="Retapez le mot de passe" required>
                    </div>
                </div>
                <div class="form-group text-center">
                    <button class="btn btn-dark" type="submit">Modifier le mot de passe</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
    include("modules/partie3.php");
?>