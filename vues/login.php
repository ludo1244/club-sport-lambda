<?php
    include("modules/partie1.php");

    if(isset($_SESSION["user"])){
        $_SESSION["error"] = "Vous etes déjà connecté a un compte";
        echo '<script type="text/javascript">';
        echo 'window.location.href="planning.php"';
        echo '</script>';
    }
?>

<div class="container card text-center">
    <h1 class="card-header">Login</h1>
    <div class="card-body">
        <form class="" action="./process/login.php" method="POST">
            <div class="row form-group">
                <label for="email" class="col-4">Email</label>
                <div class="col-md-8">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                </div>
            </div>
            <div class="row form-group">
                <label for="password" class="col-4">Mot de passe</label>
                <div class="col-md-8">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Votre mot de passe" required>
                </div>
            </div>
            <div class="row form-group">
                <a class="col-12" href="mot-de-passe-oublie.php" >mot de passe oublié</a>
            </div>
            <div class="form-group text-center">
            <button class="btn btn-dark" type="submit">Se connecter</button>
            </div>
        </form>   
    </div>
</div>
<?php
    include("modules/partie3.php")
?>