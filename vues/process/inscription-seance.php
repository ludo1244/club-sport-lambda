<?php
// sert a inscrire un user a une seance

session_start();
require_once(__DIR__."/../model/database.php");
$database = new Database();

//recupere l'id seance
$idSeance = $_GET["id"];

//recupere l'id du user
$idUser = $_SESSION["id"];

//fait l'inscription dans la BD
if($database->insertParticipant($idSeance, $idUser)){
    //Si ca c'est mal passé
    $_SESSION["info"] = "Vous avez bien été inscrit à cette séance";
}else{
    $_SESSION["error"] = "Nous n'avons pas réussi à vous inscrire à cette séance";
}
header("location: ../cours.php?id=".$idSeance);
exit();