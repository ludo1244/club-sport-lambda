<?php

//ceci remplace l'instruction quand on défini un namespace à la classe
use PHPUnit\Framework\TestCase;

include_once(__DIR__."/../vues/model/seance.php");
include_once(__DIR__."/../vues/model/user.php");
include_once(__DIR__."/../vues/model/database.php");

final class SeanceTest extends TestCase{
    public function testCreateSeance(){
        $seance = Seance::createSeance("Pilates","Ce cours détend","09:00", date("Y-m-d"),50,20,"#03bafc");
        $database = new Database();
        $this->assertNotFalse($database->createSeance($seance));
    }
    public function testGetSeanceById(){
        $database = new Database();
        $seance = Seance::createSeance("Pilates","Ce cours détend","09:00", date("Y-m-d"),50,20,"#03bafc");
        $id = $database->createSeance($seance);
        $this->assertInstanceOf(Seance::class, $database->getSeanceById($id));
    }
    public function testGetSeanceByWeek(){
        $database = new Database();
        $seance = Seance::createSeance("Pilates","Ce cours détend","09:00", date("Y-m-d"),50,20,"#03bafc");
        $this->assertNotFalse($database->createSeance($seance));
        $nbSeances = count($database->getSeanceByWeek(date("W")));
        echo($nbSeances);
        $this->assertGreaterThan(0, $nbSeances);
    }

    public function testUpdateSeance(){
        $database = new Database();
        $seance = Seance::createSeance("Pilates","Ce cours détend","09:00", date("Y-m-d"),50,20,"#03bafc");
        $id = $database->createSeance($seance);
        $seance = $database->getSeanceById($id);
        $this->assertInstanceOf(Seance::class, $seance);
        $seance->setTitre("Yoga");
        $this->assertTrue($database->updateSeance($seance));

    }
    public function testInsertDeleteParticipant(){
        $database = new Database();
        $user = User::createUser("toto","toto@gmail.com", password_hash("1234",PASSWORD_DEFAULT),0,0,bin2hex(random_bytes(20)));
        $idUser = $database->createUser($user);
        $this->assertNotFalse($idUser);

        $seance = Seance::createSeance("Pilates","Ce cours détend","09:00", date("Y-m-d"),50,20,"#03bafc");
        $idSeance = $database->createSeance($seance);
        $this->assertNotFalse($idSeance);

        $this->assertTrue($database->insertParticipant($idSeance,$idUser));

        $this->assertTrue($database->deleteParticipant($idSeance,$idUser));

    }
    public function testGetSeanceByUserId(){
        $database = new Database();
        $user = User::createUser("toto","toto@gmail.com", password_hash("1234",PASSWORD_DEFAULT),0,0,bin2hex(random_bytes(20)));
        $idUser = $database->createUser($user);
        $this->assertNotFalse($idUser);

        //verifie qu'il n'y a pas encore de seance
        $this->assertEquals(0, count($database->getSeanceByUserId($idUser)));

        $seance1 = Seance::createSeance("Pilates","Ce cours détend","09:00", date("Y-m-d"),50,20,"#03bafc");
        $idSeance1 = $database->createSeance($seance1);
        $this->assertNotFalse($idSeance1);
        // inscription de l'utilisateur a la séance
        $this->assertTrue($database->insertParticipant($idSeance1,$idUser));
        //test si il y a une séance
        $this->assertEquals(1, count($database->getSeanceByUserId($idUser)));

        $seance2 = Seance::createSeance("Yoga","Ce cours détend","10:00", date("Y-m-d"),50,20,"#03bafc");
        $idSeance2 = $database->createSeance($seance2);
        $this->assertNotFalse($idSeance2);
        $this->assertTrue($database->insertParticipant($idSeance2,$idUser));

        $this->assertEquals(2, count($database->getSeanceByUserId($idUser)));

    }


    public static function tearDownAfterClass(){
        $database = new Database();
        $database->deleteAllInscrit();
        $database->deleteAllUser();
        $database->deleteAllSeance();
    }
}
