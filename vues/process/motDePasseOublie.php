<?php
session_start();

require_once(__DIR__."/../model/database.php");
$database = new Database();


// verification de l'email
$email = isset($_POST["email"]) ? $_POST["email"] : null;


if($email == null){
    $error = "il faut rentrer un email pour modifier votre mot de passse";
}else{
    $user = $database->getUserByEmail($email);
    if ($user == null ){
        $error = "Email inconnu"; 
    }
}


if (!empty($error)){
    $_SESSION["error"] = $error;
    header("location: ../mot-de-passe-oublie.php");
    exit;
}



// mot de passe aleatoire
$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
$pass = array(); //remember to declare $pass as an array
$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
for ($i = 0; $i < 8; $i++) {
    $n = rand(0, $alphaLength);
    $pass[] = $alphabet[$n];
}
$password = implode($pass); //turn the array into a string



$passwordhash = password_hash($password,PASSWORD_DEFAULT);

if ($database->updateMDPUser($user->getId(),$passwordhash)){
    $_SESSION["info"] = "Modification mot de passe reussi, regarder votre email";

    //on ouvre un buffer, on inclut le message puis on vide le buffer dans notre variable message
    ob_start();
    include("../emails/motDePasseOublie.php");
    $message = ob_get_clean();
    $sujet = "Votre nouveau mot de passe Lambda";


    //pour que le message s'affiche en html il faut le specifier dans le header de l'email
    //on peut aussi specifier la personne qui l'envoie
    $headers = "Content-Type: text/html: charset=utf-8" . "\r\n";
    $headers .= "From: noreply@club-lambda.ch";

    //on envoie l'email
    mail($email, $sujet, $message, $headers);
    header("location: ../mot-de-passe-oublie.php");
}else{
    $_SESSION["error"] = "Modification mot de passe echoué ! veuillez reessayer";
}


header("location: ../login.php");

?>