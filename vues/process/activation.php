<?php
//ce fichier sert à activer une utilisateur qui vient de s'incrire

//session pour message
session_start();
require_once(__DIR__."/../model/database.php");
$database = new Database();

$idUser = isset($_GET["id"]) ? $_GET["id"] : null;
$token = isset($_GET["token"]) ? $_GET["token"] : null;

//verification
if($idUser == NULL || $token == NULL){
    $_SESSION["error"] = "Un problème est survenu lors de votre inscription, veuillez recommencer.";
    header("location: ../inscription.php");
    exit();
}

//on cherche l'utilisateur dans la BD à son id
$user = $database->getUserById($idUser);

if (!$user){
    $_SESSION["error"] = "Un problème est survenu lors de votre inscription, veuillez recommencer.";
    header("location: ../inscription.php");
    exit(); 
}
if($token != $user->getToken()){
    $_SESSION["error"] = "Un problème est survenu lors de votre inscription, veuillez recommencer.";
    header("location: ../inscription.php");
    exit();  
}

//si tout est bon on active l'utilisateur
if ($database->activateUser($idUser)){
    $_SESSION["info"] = "Votre compte a été activé, vous pouvez vous connecter.";
    header("location: ../login.php");
}else{
    $_SESSION["error"] = "Un problème est survenu lors de votre inscription, veuillez recommencer.";
    header("location: ../inscription.php");
}