<?php
class Commentaire{
    private $id;
    private $date;
    private $sujet;
    private $message;
    private $id_user;
    private $id_seance;

    public function __construct(){}

    public static function createCommentaire($date,$sujet,$message,$idUser, $idSeance){
        $commentaire = new self();
        $commentaire->setDate($date);
        $commentaire->setSujet($sujet);
        $commentaire->setMessage($message);
        $commentaire->setIdUser($idUser);
        $commentaire->setIdSceance($idSeance);
        return $commentaire;
    }
    //getters
    public function getId(){return $this->id;}
    public function getDate(){return $this->date;}
    public function getSujet(){return $this->sujet;}
    public function getMessage(){return $this->message;}
    public function getIdUser(){return $this->id_user;}
    public function getIdSeance(){return $this->id_seance;}

    //setters
    public function setId($id){$this->id = $id;}
    public function setDate($date){$this->date = $date;}
    public function setSujet($sujet){$this->sujet = $sujet;}
    public function setMessage($message){$this->message = $message;}
    public function setIdUser($idUser){$this->id_user = $idUser;}
    public function setIdSceance($idSeance){$this->id_seance = $idSeance;}
}