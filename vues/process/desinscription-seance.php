<?php
// sert a inscrire un user a une seance

session_start();
require_once(__DIR__."/../model/database.php");
$database = new Database();

$idSeance = $_GET["id"];

$idUser = $_SESSION["id"];


//effectuer la deinscription en base de données
if($database->deleteParticipant($idSeance, $idUser)){
    $_SESSION["info"] = "Vous avez bien été désinscrit de cette séance";
}else{
    $_SESSION["error"] = "Nous n'avons pas réussi à vous déinscrire de cette séance";
}
header("location: ../cours.php?id=".$idSeance);
exit();