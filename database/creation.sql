-- Script de création de la base de données
-- création de la base de données
CREATE DATABASE clubSportLambda;

-- Utilisons cette nouvelle base de données
USE clubSportLambda;

-- Création d'un utilisateur admin 
-- et on lui donne tous les droits sur la base de données
-- et sur toutes les tables de la base
CREATE USER 'adminClub'@'%' IDENTIFIED BY '2Bit8DL9x';
GRANT ALL PRIVILEGES ON clubSportLambda.* TO 'adminClub'@'%';