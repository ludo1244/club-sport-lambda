<?php
    include("modules/partie1.php");
?>

<?php 
    require_once(__DIR__."/../vues/model/database.php");
    $database = new Database();

    //recupere l'id dans l'url ou prend 1 par défaut
    $idSeance = isset($_GET["id"]) ? $_GET["id"] : 1;

    //Je vais chercher la séance dans la base de données
    $seance = $database->getSeanceById($idSeance);

    $user = unserialize($_SESSION["user"]);

    $nbInscrits = $database->nombreInscrit($idSeance);
    if (isset($_SESSION["user"])){
        $isInscrit = $database->isInscrit($user->getId(), $idSeance);
    }
    $commentaires = $database->getCommentaireByIdSeance($idSeance);
?>

<div class="container card text-center mt-4">
    <h1 class="card-header"><?php echo $seance->getTitre() ?></h1>
    <div class="card-body text-left" style="background: <?php echo $seance->getCouleur()?>;">
        <div class="row">
            <div class="offset-3 col-9">
                <p>Date : <?php echo $seance->getDate() ?></p>
            </div>
            <div class="offset-3 col-9">
                <p>Heure de début : <?php echo $seance->getHeureDebut() ?></p>
            </div>
            <div class="offset-3 col-9">
                <p>Durée : <?php echo $seance->getDuree() ?> minutes</p>
            </div>
            <div class="offset-3 col-9">
                <p>Description :</p>
            </div>
            <div class="offset-3 col-9">
                <p><?php echo $seance->getDescription()?> </p>
            </div>
            <div class="offset-3 col-9">
                <p>Nombre de participants max : <?php echo $seance->getNbParticipantsMax() ?></p>
            </div>
            <?php if (isset($_SESSION["user"])){ ?>
                <div class="col-12">
                    <div class="d-flex justify-content-around m-2">
                        <?php if($isInscrit){ ?>
                            <a class="btn btn-danger" href="process/desinscription-seance.php?id=<?php echo $seance->getId();?>">Se désinscrire</a>
                        <?php }elseif($nbInscrits < $seance->getNbParticipantsMax()){ ?>
                            <a class="btn btn-primary" href="process/inscription-seance.php?id=<?php echo $seance->getId();?>">S'inscrire</a>
                        <?php }else{ ?>
                            <a class="btn btn-danger" href="#">Complet</a>
                        <?php } ?>
                    </div>
                </div>
                <?php  if($user->getAdmin() == 1) { ?>
                    <div class="col-12">
                        <div class="d-flex justify-content-around m-2">
                            <a class="btn btn-warning" href="/vues/formulaire.php?id=<?php echo $seance->getId();?>&type=3">Dupliquer</a>
                            <a class="btn btn-primary" href="/vues/formulaire.php?id=<?php echo $seance->getId();?>&type=2">Modifier</a>
                            <a class="btn btn-danger" href="/vues/process/delete-seance.php?id=<?php echo $seance->getId();?>">Supprimer</a>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>

<!-- les commentaire sur le cours -->
<div class="container card text-center mt-4">
    <h1 class="card-header">Commentaires</h1>
    <div class="card-body text-left">
        <div class="row">
            <div class="col-12">
                <?php 
                if ($commentaires){
                foreach($commentaires as $commentaire){
                    $tmpUser = $database->getUserById($commentaire->getIdUser());
                ?>
                    <div class="card text-center mt-4">
                    <h5 class="card-title"><?php echo $commentaire->getSujet(); ?></h5>
                        <div class="card-body text-left">
                            <div class="row">
                                <div class="col-3 backgroundOrange">
                                    <?php 
                                    echo $commentaire->getDate();
                                    echo "  ";
                                    echo $tmpUser->getNom();
                                    ?>
                                </div>
                                <div class=" col-8">
                                    <?php echo $commentaire->getMessage(); ?>
                                </div>
                                <?php if($user != null){if($user->getId()==$tmpUser->getId()||$user->getAdmin()==1){ ?>
                                    <div class="col-1">
                                        <a class="btn btn-danger" href="process/delete-commentaire.php?id=<?php echo $commentaire->getId();?>">X</a>
                                    </div>
                                <?php }} ?>
                            </div>
                        </div>
                    </div>
                <?php } } ?>
                <!-- envoie du commentaire -->
                <?php if(isset($_SESSION["user"])){ ?>
                <div class="card text-center mt-4">
                    <div class="card-body text-left">
                        <form class="text-md-right" action="process/commentaire.php?idSceance=<?php echo $idSeance ?>" method="POST">
                            <div class="form-group row">
                                <label for="sujet" class="col-md-4 col-form-label">Sujet</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="sujet" name="sujet" placeholder="Sujet" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="message" class="col-md-4 col-form-label">Commentaire</label>
                                <div class="col-md-8">
                                    <textarea type="text" class="form-control" id="message" name="message" placeholder="Commentaire" required></textarea>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button class="btn btn-dark" type="submit">Envoyer</button>
                            </div>
                        </form>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php
    include("modules/partie3.php");
?>    