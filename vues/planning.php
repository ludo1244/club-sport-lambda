<?php
    include("modules/partie1.php");

    require_once(__DIR__."/../vues/model/database.php");
    $database = new Database();

    if ($_GET["week"] == null){
        $week = date("W");
    }else{
        $week = $_GET["week"];
    }
    $seancesOfWeek = $database->getSeanceByWeek($week);

    const LUNDI = 1;
    const MARDI = 2;
    const MERCREDI = 3;
    const JEUDI = 4;
    const VENDREDI = 5;
    const SAMEDI = 6;

    $seances = [];
    $seances[LUNDI] = [];
    $seances[MARDI] = [];
    $seances[MERCREDI] = [];
    $seances[JEUDI] = [];
    $seances[VENDREDI] = [];
    $seances[SAMEDI] = [];

    foreach($seancesOfWeek as $seance){
        $indexDay = date("w", strtotime($seance->getDate()));
        array_push($seances[$indexDay],$seance);
    }

?>

<div class="container card text-center">
    <h1 class="card-header">Planning de la semaine</h1>
    <div class="card-body">
        <div class="row d-flex justify-content-around mb-3">
            <a class="btn btn-dark" href="planning.php?week=<?php echo $week-1 ?>"><-</a>
            <a class="btn btn-dark" href="planning.php">Cette semaine</a>
            <a class="btn btn-dark" href="planning.php?week=<?php echo $week+1 ?>">-></a>
        </div>
        <div class="row">
            <div id="lundi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>LUNDI</h3>
                <?php
                    foreach($seances[LUNDI] as $seance){
                        include("modules/etiquette.php");
                    }
                ?>
            </div>
            <div id="mardi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>MARDI</h3>
                <?php
                    foreach($seances[MARDI] as $seance){
                        include("modules/etiquette.php");
                    }
                ?>
            </div>
            <div id="mercredi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>MERCREDI</h3>
                <?php
                    foreach($seances[MERCREDI] as $seance){
                        include("modules/etiquette.php");
                    }
                ?>
            </div>
            <div id="jeudi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>JEUDI</h3>
                <?php
                    foreach($seances[JEUDI] as $seance){
                        include("modules/etiquette.php");
                    }
                ?>
            </div>
            <div id="vendredi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>VENDREDI</h3>
                <?php
                    foreach($seances[VENDREDI] as $seance){
                        include("modules/etiquette.php");
                    }
                ?>
            </div>
            <div id="samedi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>SAMEDI</h3>
                <?php
                    foreach($seances[SAMEDI] as $seance){
                        include("modules/etiquette.php");
                    }
                ?>
            </div>
        </div>
    </div>
</div>
<?php
    include("modules/partie3.php");