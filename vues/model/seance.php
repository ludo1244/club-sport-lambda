<?php
class Seance{
    private $id;
    private $titre;
    private $description;
    private $heureDebut;
    private $date;
    private $duree;
    private $nbParticipantsMax;
    private $couleur;

    public function __construct(){}

    public static function createSeance($titre,$description,$heureDebut,$date, $duree, $nbrParticipantsMax,$couleur){
        $seance = new self();
        $seance->setTitre($titre);
        $seance->setDescription($description);
        $seance->setHeureDebut($heureDebut);
        $seance->setDate($date);
        $seance->setDuree($duree);
        $seance->setNbParticipantsMax($nbrParticipantsMax);
        $seance->setCouleur($couleur);
        return $seance;
    }
    //getters
    public function getId(){return $this->id;}
    public function getTitre(){return $this->titre;}
    public function getDescription(){return $this->description;}
    public function getHeureDebut(){return $this->heureDebut;}
    public function getDate(){return $this->date;}
    public function getDuree(){return $this->duree;}
    public function getNbParticipantsMax(){return $this->nbParticipantsMax;}
    public function getCouleur(){return $this->couleur;}

    //setters
    public function setId($id){$this->id = $id;}
    public function setTitre($titre){$this->titre = $titre;}
    public function setDescription($description){$this->description = $description;}
    public function setHeureDebut($heureDebut){$this->heureDebut = $heureDebut;}
    public function setDate($date){$this->date = $date;}
    public function setDuree($duree){$this->duree = $duree;}
    public function setNbParticipantsMax($nbParticipantsMax){$this->nbParticipantsMax = $nbParticipantsMax;}
    public function setCouleur($couleur){$this->couleur = $couleur;}
}