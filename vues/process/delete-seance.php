<?php
// ce fichier sert a supprimer une séance
//session pour passer les message d'une page a l'autre
session_start();

require_once("../model/database.php");
$database = new Database();

$idSeance = $_GET["id"];

//verifie si on a un id
if(!$idSeance){
    $_SESSION["error"] = "Le lien de suppression n'est pas correct";
    header("location: ../planning.php");
}
// si tout vas bien on supprime la seance
if ($database->deleteSeance($idSeance)){
    $_SESSION["info"] = "Séance supprimée avec succès";
    header("location: ../planning.php");
}else{
    $_SESSION["error"] = "Le lien de suppression n'est pas correct";
    header("location: ../planning.php");
}

?>