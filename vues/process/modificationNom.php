<?php
session_start();

require_once(__DIR__."/../model/database.php");
$database = new Database();

$user = unserialize($_SESSION["user"]);

$nom = isset($_POST["name"]) ? $_POST["name"] : null;

if($nom){
    if ($database->updateNameUser($user->getId(),$nom)){
        $_SESSION["info"] = "Modification reussi";
        $user = $database->getUserById($user->getId());
        $_SESSION["user"] = serialize($user);
    }else{
        $_SESSION["error"] = "Modification echoué ! veuillez reessayer";
    }
}

header("location: ../profil.php");

?>