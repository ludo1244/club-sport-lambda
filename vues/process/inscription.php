<?php
// fichier pour process les données du formulaire

//demarrer la session pour les messages
session_start();

require_once(__DIR__."/../model/database.php");
$database = new Database();

$nom = isset($_POST["name"]) ? $_POST["name"] : null;
$email = isset($_POST["email"]) ? $_POST["email"] : null;
$password = isset($_POST["password"]) ? $_POST["password"] : null;
$passwordRepeat = isset($_POST["password-repeat"]) ? $_POST["password-repeat"] : null;
$admin = isset($_POST["admin"]) ? 1 : 0;


//validation des données recus
$error = "";
if($nom == null){
    $error .= "Le nom d'utilisateur doit être rempli";
}
if($email == null){
    $error .= "L'email' doit être renseigné";
}
if($database->isEmailExists($email)){
    $error .= "Cet email existe déjà";
}
if($password == null){
    $error .= "Le mot de passe est obligatoire";
}
if($passwordRepeat == null || $passwordRepeat != $password){
    $error .= "Vous devez repéter le même mot de passe";
}

//en cas d'erreurs rediriger vers le formulaire
if (!empty($error)){
    $_SESSION["error"] = $error;
    header("location: ../inscription.php");
    exit;
}

// s'il n'y pas d'erreur on crée l'utilisateur en mode inactif
// on stock le token pour pouvoir le reutiliser
$token = bin2hex(random_bytes(20));
$user = User::createUser($nom, $email, password_hash($password,PASSWORD_DEFAULT),$admin,0,$token);

//on sauvegarde l'utilisateur et on recupere l'id
$idUser = $database->createUser($user);

// on doit envoyer un email
if ($idUser){
    //on ouvre un buffer, on inclut le message puis on vide le buffer dans notre variable message
    ob_start();
    include("../emails/activation.php");
    $message = ob_get_clean();
    $sujet = "Activer votre compte Lambda";

    //pour que le message s'affiche en html il faut le specifier dans le header de l'email
    //on peut aussi specifier la personne qui l'envoie
    $headers = "Content-Type: text/html: charset=utf-8" . "\r\n";
    $headers .= "From: noreply@club-lambda.ch";

    //on envoie l'email
    mail($email, $sujet, $message, $headers);

    //on redirige vers l'ecran de login avec un message
    $_SESSION["info"] = "Votre inscription a bien été prise en compte, vous avez recu un email pour activer votre compte";
    header("location: ../login.php");
}else{
    $_SESSION["error"] = "Une erreur s'est produite lors de votre inscription. veuillez recommencer le processus.";
    header("location: ../inscription.php");
}