<?php
    include("modules/partie1.php");

    if(isset($_SESSION["user"])){
        $user = unserialize($_SESSION["user"]);
        if($user->getAdmin() == 0) {
            $_SESSION["error"] = "Vous etes déjà connecté a un compte";
            echo '<script type="text/javascript">';
            echo 'window.location.href="planning.php"';
            echo '</script>'; 
        }
    }
?>
<div class="container card text-center">
    <h1 class="card-header">Inscription</h1>
    <div class="card-body">
        <form class="text-md-right" action="process/inscription.php" method="POST">
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label">Nom</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nom et prénom" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label">Email</label>
                <div class="col-md-8">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label">Mot de passe</label>
                <div class="col-md-8">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="password-repeat" class="col-md-4 col-form-label">Retapez le mot de passe</label>
                <div class="col-md-8">
                    <input type="password" class="form-control" id="password-repeat" name="password-repeat" placeholder="Retapez le mot de passe" required>
                </div>
            </div>
            <?php if(isset($_SESSION["user"])){ if($user->getAdmin() == 1){ ?>
            <div class="form-group row">
                <label for="admin" class="col-md-4 col-form-label">compte admin</label>
                <div class="col-md-8 text-left">
                    <input type="checkbox" id="admin" name="admin">
                </div>
            </div>
            <?php } } ?>
            <div class="form-group text-center">
                <button class="btn btn-dark" type="submit">S'inscrire</button>
            </div>
        </form>
    </div>
</div>
<?php
    include("modules/partie3.php");
?>