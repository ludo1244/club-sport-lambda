<?php
class User{
    private $id;
    private $nom;
    private $email;
    private $password;
    private $isAdmin;
    private $isActif;
    private $token;

    public function __construct(){}

    public static function createUser($nom,$email,$password,$isAdmin, $isActif, $token){
        $user = new self();
        $user->setNom($nom);
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setAdmin($isAdmin);
        $user->setActif($isActif);
        $user->setToken($token);
        return $user;
    }
    //getters
    public function getId(){return $this->id;}
    public function getNom(){return $this->nom;}
    public function getEmail(){return $this->email;}
    public function getPassword(){return $this->password;}
    public function getAdmin(){return $this->isAdmin;}
    public function getActif(){return $this->isActif;}
    public function getToken(){return $this->token;}

    //setters
    public function setId($id){$this->id = $id;}
    public function setNom($nom){$this->nom = $nom;}
    public function setEmail($email){$this->email = $email;}
    public function setPassword($password){$this->password = $password;}
    public function setAdmin($isAdmin){$this->isAdmin = $isAdmin;}
    public function setActif($isActif){$this->isActif = $isActif;}
    public function setToken($token){$this->token = $token;}
}