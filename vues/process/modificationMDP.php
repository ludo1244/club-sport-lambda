<?php
session_start();

require_once(__DIR__."/../model/database.php");
$database = new Database();

$user = unserialize($_SESSION["user"]);

$password = isset($_POST["password"]) ? $_POST["password"] : null;
$passwordRepeat = isset($_POST["password-repeat"]) ? $_POST["password-repeat"] : null;

if($password == null){
    $error .= "Le mot de passe est obligatoire";
}
if($passwordRepeat == null || $passwordRepeat != $password){
    $error .= "Vous devez repéter le même mot de passe";
}
//en cas d'erreurs rediriger vers le formulaire
if (!empty($error)){
    $_SESSION["error"] = $error;
    header("location: ../profil.php");
    exit;
}

$password = password_hash($password,PASSWORD_DEFAULT);

if ($database->updateMDPUser($user->getId(),$password)){
    $_SESSION["info"] = "Modification mot de passe reussi";
}else{
    $_SESSION["error"] = "Modification mot de passe echoué ! veuillez reessayer";
}


header("location: ../profil.php");

?>